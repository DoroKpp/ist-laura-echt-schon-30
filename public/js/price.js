window.addEventListener('DOMContentLoaded', () => {
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    var winNumber = urlParams.get('number');
    var imageUrl, title, qrCode = undefined;

    switch (winNumber) {
        case "266":
            imageUrl = "images/IMG_6877.JPG";
            title = "ERINNERUNG!"
            break;
        case "021":
            imageUrl = "images/IMG_0018.PNG";
            title = "ERINNERUNG!"
            break;
        case "131":
            title = "NIETE!"
            break;
        case "001":
            imageUrl = "images/gutschein1.jpg";
            qrCode = true;
            title = "GESCHENK!"
            break;
        case "100":
            imageUrl = "images/corgis1.gif";
            title = "CORGIS!"
            break;
        case "119":
            imageUrl = "images/IMG_9969.jpg";
            title = "ERINNERUNG!"
            break;
        case "136":
            imageUrl = "images/IMG_0442.jpg";
            title = "ERINNERUNG!"
            break;
        case "070":
            imageUrl = "images/corgis2.gif";
            title = "CORGIS!"
            break;
        case "081":
            imageUrl = "images/IMG_7009.jpg";
            title = "ERINNERUNG!"
            break;
        case "190":
            imageUrl = "images/gutschein20.jpg";
            qrCode = true;
            title = "GESCHENK!"
            break;
        case "061":
            imageUrl = "images/IMG_0504.JPEG";
            title = "ERINNERUNG!"
            break;
        case "201":
            imageUrl = "images/2374beb2-aa69-4bbd-8470-64013aece4d2.jpg";
            title = "ERINNERUNG!"
            break;
        case "154":
            imageUrl = "images/IMG_7468.JPG";
            title = "ERINNERUNG!"
            break;
        case "132":
            imageUrl = "images/IMG_6781.JPG";
            title = "ERINNERUNG!"
            break;
        case "141":
            imageUrl = "images/gutschein31.jpg";
            qrCode = true;
            title = "GESCHENK!"
            break;
        case "003":
            title = "NIETE!"
            break;
        case "257":
            imageUrl = "images/IMG_3666.jpg";
            title = "ERINNERUNG!"
            break;
        case "222":
            imageUrl = "images/2c923012-7060-41de-abda-0d37cc85aa24.jpg";
            title = "ERINNERUNG!"
            break;
        case "727":
            imageUrl = "images/IMG_8959.jpg";
            title = "ERINNERUNG!"
            break;
        case "044":
            imageUrl = "images/FullSizeRender.jpg";
            title = "ERINNERUNG!"
            break;
        case "233":
            imageUrl = "images/corgis3.gif";
            title = "CORGIS!"
            break;
        case "726":
            imageUrl = "images/IMG_1464.JPG";
            title = "ERINNERUNG!"
            break;
        case "211":
            title = "NIETE!"
            break;
        case "002":
            imageUrl = "images/IMG_6016.jpg";
            title = "ERINNERUNG!"
            break;
        case "110":
            imageUrl = "images/IMG_8970.jpg";
            title = "ERINNERUNG!"
            break;
        case "224":
            imageUrl = "images/Gutschein04.jpg";
            qrCode = true;
            title = "GESCHENK!"
            break;
        case "501":
            imageUrl = "images/IMG_4160.JPG";
            title = "ERINNERUNG!"
            break;
        case "777":
            imageUrl = "images/corgis4.gif";
            title = "CORGIS!"
            break;
        case "151":
            title = "NIETE!"
            break;
        case "102":
            imageUrl = "images/gutschein55.jpg";
            qrCode = true;
            title = "GESCHENK!"
            break;
        case "316":
            imageUrl = "images/IMG_5187.jpg";
            title = "ERINNERUNG!"
            break;
        case "180":
            imageUrl = "images/corgis5.gif";
            title = "CORGIS!"
            break;
        case "333":
            imageUrl = "images/6766d3b0-6cda-4d15-892f-eba762e63788.jpg";
            title = "ERINNERUNG!"
            break;
        case "217":
            imageUrl = "images/IMG_5455.jpg";
            title = "ERINNERUNG!"
            break;
    }

    document.querySelector("#title").innerText = title;

    if (imageUrl) {
        document.querySelector("#image").src = imageUrl;
    } else {
        document.querySelector("#image").style.display = 'none';
    }

    if (qrCode) {
        document.querySelector("#qrbox").style.display = "flex";
    } else {
        document.querySelector("#qrbox").style.display = "none";
    }



});
